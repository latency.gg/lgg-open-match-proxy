package lgg

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"
)

type Beacon struct {
	IPV4 string `json:"ipv4"`
	IPV6 string `json:"ipv6"`
}

type Metrics struct {
	Beacons []Beacon `json:"beacons"`
	RTT     int      `json:"rtt"`
	Stddev  int      `json:"stddev"`
	Stale   bool     `json:"stale"`
}

type MetricsProvider interface {
	Get(provider, location, ip string) (*Metrics, error)
}

type DummyMetricsProvider struct {
	metrics Metrics
}

type HttpMetricsProvider struct {
	client   *http.Client
	BaseUrl  string
	Username string
	Password string
}

func NewHttpMetricsProvider(
	baseUrl, username, password string,
	timeout time.Duration,
) MetricsProvider {
	return &HttpMetricsProvider{
		client: &http.Client{
			Timeout: timeout,
		},

		BaseUrl:  baseUrl,
		Username: username,
		Password: password,
	}
}

func NewDummyMetricsProvider(metrics Metrics) MetricsProvider {
	return &DummyMetricsProvider{metrics}
}

func (p *HttpMetricsProvider) Get(provider, location, ip string) (*Metrics, error) {
	relativePath := fmt.Sprintf("metrics/%v/%v/%v", provider, location, ip)

	u := url.URL{
		Scheme: "https",
		Host:   p.BaseUrl,
		Path:   relativePath,
	}

	bytes, err := httpGetWithBasicAuth(p.client, u, p.Username, p.Password)
	if err != nil {
		return nil, err
	}

	metrics := &Metrics{}

	err = json.Unmarshal(bytes, metrics)
	if err != nil {
		return nil, err
	}

	return metrics, nil
}

func (p *DummyMetricsProvider) Get(provider, location, ip string) (*Metrics, error) {
	return &p.metrics, nil
}
