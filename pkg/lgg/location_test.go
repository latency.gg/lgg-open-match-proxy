package lgg

import (
	"errors"
	"os"
	"testing"
	"time"
)

func TestGetProvider(t *testing.T) {
	var (
		username = os.Getenv("LGG_CLIENT_USERNAME")
		password = os.Getenv("LGG_CLIENT_PASSWORD")
	)

	provider := NewHttpLocationProvider("client.latency.gg", username, password, 15*time.Second)
	res, err := provider.Get("aws")
	if err != nil {
		t.Error(err)
	}

	if len(res.Locations) == 0 {
		t.Error(errors.New("expected list of locations"))
	}
}

func TestGetAllProviders(t *testing.T) {
	var (
		username = os.Getenv("LGG_CLIENT_USERNAME")
		password = os.Getenv("LGG_CLIENT_PASSWORD")
	)

	provider := NewHttpLocationProvider("client.latency.gg", username, password, 15*time.Second)
	providers, err := provider.GetAll()
	if err != nil {
		t.Error(err)
	}

	if len(providers) == 0 {
		t.Error(errors.New("expected list of providers"))
	}
}
