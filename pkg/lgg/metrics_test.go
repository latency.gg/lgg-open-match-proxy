package lgg

import (
	"errors"
	"os"
	"testing"
	"time"
)

func TestGetMetrics(t *testing.T) {
	var (
		username = os.Getenv("LGG_CLIENT_USERNAME")
		password = os.Getenv("LGG_CLIENT_PASSWORD")
	)

	provider := NewHttpMetricsProvider("client.latency.gg", username, password, 15*time.Second)

	metrics, err := provider.Get("aws", "aws-eu-west-1", "35.68.21.42")
	if err != nil {
		t.Error(err)
	}

	if len(metrics.Beacons) == 0 {
		t.Error(errors.New("expected list of beacons"))
	}
}
