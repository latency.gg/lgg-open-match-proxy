package grpc

import (
	"context"

	"gitlab.com/latency.gg/lgg-open-match-proxy/pkg/intercept"
	"gitlab.com/latency.gg/lgg-open-match-proxy/pkg/lgg"
	"open-match.dev/open-match/pkg/pb"
)

type CreateTicket func(ctx context.Context, req *pb.CreateTicketRequest) (*pb.Ticket, error)

type Proxy struct {
	pb.UnimplementedFrontendServiceServer
	client pb.FrontendServiceClient

	metricsProvider  lgg.MetricsProvider
	locationProvider lgg.LocationProvider
	createTicket     CreateTicket
}

func NewProxy(
	client pb.FrontendServiceClient,
	metricsProvider lgg.MetricsProvider,
	locationProvider lgg.LocationProvider,
	createTicket CreateTicket,
) *Proxy {
	return &Proxy{
		client:           client,
		metricsProvider:  metricsProvider,
		locationProvider: locationProvider,
		createTicket:     createTicket,
	}
}

func (p *Proxy) CreateTicket(ctx context.Context, req *pb.CreateTicketRequest) (*pb.Ticket, error) {
	if p.createTicket != nil {
		// only here for the sake of testability
		return p.createTicket(ctx, req)
	}

	err := intercept.Do(req.Ticket, p.metricsProvider, p.locationProvider)
	if err != nil {
		return nil, err
	}

	resp, err := p.client.CreateTicket(ctx, req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
