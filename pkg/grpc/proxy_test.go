package grpc

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	"gitlab.com/latency.gg/lgg-open-match-proxy/pkg/lgg"
	spec "gitlab.com/latency.gg/lgg-open-match-spec/pkg/pb"
	"google.golang.org/grpc"
	"open-match.dev/open-match/pkg/pb"
)

func TestGrpcFrontendServiceCall(t *testing.T) {
	conn, err := grpc.Dial("localhost:50504", grpc.WithInsecure())
	if err != nil {
		t.Error(err)
		return
	}

	client := pb.NewFrontendServiceClient(conn)

	extensions := make(map[string]*any.Any)

	ipAddr, err := ptypes.MarshalAny(&spec.IPAddress{
		Value: "62.163.0.35",
	})
	if err != nil {
		t.Error(err)
	}

	extensions["ip_address"] = ipAddr

	resp, err := client.CreateTicket(context.Background(), &pb.CreateTicketRequest{
		Ticket: &pb.Ticket{},
	})
	log.Println(resp)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestMyDirector(t *testing.T) {
	conn, err := grpc.Dial("localhost:50505", grpc.WithInsecure())
	if err != nil {
		t.Error(err)
		return
	}

	client := pb.NewBackendServiceClient(conn)

	var matches []*pb.Match

	req := &pb.FetchMatchesRequest{
		Config: &pb.FunctionConfig{
			Host: "om-function.open-match-demo.svc.cluster.local",
			Port: 50502,
			Type: pb.FunctionConfig_GRPC,
		},
		Profile: &pb.MatchProfile{
			Name: "1v1",
			Pools: []*pb.Pool{
				{
					Name: "Everyone",
				},
			},
		},
	}

	stream, err := client.FetchMatches(context.Background(), req)
	if err != nil {
		panic(err)
	}

	for {
		resp, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		matches = append(matches, resp.GetMatch())
	}
}

func TestProxy(t *testing.T) {
	var (
		username = os.Getenv("LGG_CLIENT_USERNAME")
		password = os.Getenv("LGG_CLIENT_PASSWORD")
	)

	var locationProvider = lgg.NewHttpLocationProvider("client.latency.gg", username, password, 5*time.Second)
	var metricsProvider = lgg.NewHttpMetricsProvider("client.latency.gg", username, password, 5*time.Second)

	var echoCreateTicket CreateTicket = func(ctx context.Context, req *pb.CreateTicketRequest) (*pb.Ticket, error) {
		return req.Ticket, nil
	}

	remoteProxy := NewProxy(nil, nil, nil, echoCreateTicket)

	err := createFrontendServer(remoteProxy, 48080)
	if err != nil {
		t.Error(err)
	}

	remoteClient, err := createFrontendClient(48080)
	if err != nil {
		t.Error(err)
	}

	localProxy := NewProxy(remoteClient, metricsProvider, locationProvider, nil)
	err = createFrontendServer(localProxy, 48081)
	if err != nil {
		t.Error(err)
	}

	localClient, err := createFrontendClient(48081)
	if err != nil {
		t.Error(err)
	}

	extensions := make(map[string]*any.Any)

	ipAddr, err := ptypes.MarshalAny(&spec.IPAddress{
		Value: "62.163.0.35",
	})
	if err != nil {
		t.Error(err)
	}

	extensions["ip_address"] = ipAddr

	res, err := localClient.CreateTicket(context.Background(), &pb.CreateTicketRequest{
		Ticket: &pb.Ticket{
			Extensions: extensions,
		},
	})

	if err != nil {
		t.Error(err)
	}

	if res == nil {
		t.Fail()
	}
}

func createFrontendClient(port int) (pb.FrontendServiceClient, error) {
	conn, err := grpc.Dial(fmt.Sprint("localhost:", port), grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	client := pb.NewFrontendServiceClient(conn)
	return client, nil
}

func createFrontendServer(proxy *Proxy, port int) error {
	lis, err := net.Listen("tcp", fmt.Sprint(":", port))
	if err != nil {
		return err
	}

	go func() {
		s := grpc.NewServer()
		pb.RegisterFrontendServiceServer(s, proxy)
		if err := s.Serve(lis); err != nil {
			return
		}
	}()

	return nil
}
