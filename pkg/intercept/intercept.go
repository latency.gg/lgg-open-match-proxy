package intercept

import (
	"github.com/golang/protobuf/ptypes"
	"gitlab.com/latency.gg/lgg-open-match-proxy/pkg/lgg"
	spec "gitlab.com/latency.gg/lgg-open-match-spec/pkg/pb"
	"open-match.dev/open-match/pkg/pb"
)

func Do(
	ticket *pb.Ticket,
	metricsProvider lgg.MetricsProvider,
	locationProvider lgg.LocationProvider,
) error {
	ipAddressExt := ticket.Extensions["ip_address"]
	if ipAddressExt == nil {
		return nil
	}

	ipAddress := &spec.IPAddress{}

	err := ptypes.UnmarshalAny(ipAddressExt, ipAddress)
	if err != nil {
		return err
	}

	var calculations []*spec.Calculation

	providers, err := locationProvider.GetAll()
	if err != nil {
		return err
	}

	for _, provider := range providers {
		for _, location := range provider.Locations {
			resp, err := metricsProvider.Get(provider.Name, location, ipAddress.Value)
			if err != nil {
				return err
			}

			calculations = append(calculations, &spec.Calculation{
				Provider:      provider.Name,
				Location:      location,
				RoundTripTime: int32(resp.RTT),
			})
		}
	}

	calculationSet, err := ptypes.MarshalAny(&spec.CalculationSet{
		Calculations: calculations,
	})

	if err != nil {
		return err
	}

	ticket.Extensions["lgg_calculations"] = calculationSet

	return nil
}
