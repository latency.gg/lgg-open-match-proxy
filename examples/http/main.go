package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/any"
	spec "gitlab.com/latency.gg/lgg-open-match-spec/pkg/pb"
	"open-match.dev/open-match/pkg/pb"
)

func main() {
	extensions := make(map[string]*any.Any)
	ipAddr, err := ptypes.MarshalAny(&spec.IPAddress{
		Value: "<INSERT_A_CLIENTS_IP_ADDRESS_HERE>",
	})

	if err != nil {
		log.Fatal(err)
	}

	extensions["ip_address"] = ipAddr

	req := &pb.CreateTicketRequest{
		Ticket: &pb.Ticket{
			Extensions: extensions,
		},
	}

	client := &http.Client{}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}

	// assuming that the proxy has been portforwarded
	httpReq, err := http.NewRequest(http.MethodPost, "http://127.0.0.1:8080", bytes.NewBuffer(reqBytes))
	if err != nil {
		log.Fatal(err)
	}

	_, err = client.Do(httpReq)
	if err != nil {
		log.Fatal(err)
	}
}
