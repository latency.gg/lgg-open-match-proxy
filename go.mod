module gitlab.com/latency.gg/lgg-open-match-proxy

go 1.17

require (
	github.com/golang/protobuf v1.5.2
	gitlab.com/latency.gg/lgg-open-match-spec v0.0.1
	google.golang.org/grpc v1.45.0
	open-match.dev/open-match v1.3.0
)

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.3.0 // indirect
	golang.org/x/net v0.0.0-20210224082022-3d97a244fca7 // indirect
	golang.org/x/sys v0.0.0-20210225134936-a50acf3fe073 // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/genproto v0.0.0-20210224155714-063164c882e6 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)
